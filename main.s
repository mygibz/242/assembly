.equ Zero, 2

.global ManchesterEncode


ManchesterEncode:
	ldi r20, 2		;-- initialize the mask to toggle one of the encoded bits; needed since there is not instruction eori
	ldi r21, 8		;-- initialize loop counter; 
	mov r22, r24	;-- copy parameter to r22 --> now we can use r24 to
	eor r24, r24    ;-- set registers to 0
	eor r25, r25
loop_me:
	lsl r24			;-- contains result low byte left shift twice
	rol r25
	lsl r24
	rol r25
	bst r22, 0      ;-- store bit 0 of parameter data in T-bit of status register
	bld r24, 1      ;-- resostore the T-bit bit @ position 1 of the result
	bld r24, 0     	;-- resostore the T-bit bit @ position 0 of the result
	eor r24, r20    ;-- xor the result with the value 2 (invert the bit 1 of the result)
	lsr r22		   	;-- shift out the 0 bit
	dec r21        	;-- decrement loop counter; this sets the status bits!
	brne loop_me	;-- branch if the counter has reached 0
	ret
